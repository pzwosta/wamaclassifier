/***
 * Create new reminder on  echo devices triggered by a state change to true on
 * another device.
 * 
 * */
        
 const wamaReminderOptions = {
    delayInMinutes: 1,
    message: "Die Waschmaschine ist gleich fertig!",
    triggeredByState: "0_userdata.0.WamaCamera.state", 
    echoObjectId: "alexa2.0.Echo-Devices.XXXXXXXXXXXXXXXX", 
    states: ["00_10plusminleft", "01_9minleft", "02_off"],
    initialState: 2,
    triggerState: 1,
    triggerOldState: 0,
    noReminderPeriods: [{ begin: "22:00", end: "09:00"}]
  };

class WamaReminder {

    // default values - don't touch, define your own wamaReminderOptions! and pass them to constructor - see the last line of this script
    reminderOptions = {
        delayInMinutes: 1, // Set Remind time xx minutes from now 
        message: "Hello", // Message to be spoken by the echo device
        triggeredByState: "", // State that triggers the script, 0_userdata.0 ist fixed (see setUserdataState)
        echoObjectId: "", // Echo device id, like: alexa2.0.Echo-Devices.90F008395732619UG
        states: [], // use the states from labels.txt of your model
        initialState: 0, // Nr of the state to be set when starting the script - default 0
        triggerState: 0, // Nr of the state that triggers the reminder
        triggerOldState: 0, // Nr of the state previous to trigger state
        noReminderPeriods: [], // Don't set a reminder during these periods i.e.: [{ begin: "12:00", end: "13:00" }, { begin: "23:00", end: "05:00" }]
    };

    // see echo reminder Objects in alexa2.0.Echo-Devices.<device-id>.Reminder
    reminderTags = {
        id: "Reminder",
        newFnId: "New",
        timeId: "time", // see Reminder.<reminder-id>
    };
        

    constructor(options) {
        this.reminderOptions = options;     
        this.setUserdataState(this.reminderOptions.states[this.reminderOptions.initialState])
        
        // This is just for testing purposes if you want an easy trigger 
        // -> uncomment this, replace the "on"-block below 
        //    setTimeout(() => {
        //        var obj = {
        //               state: { val: this.reminderOptions.states[this.reminderOptions.triggerState] },
        //               oldState: { val: this.reminderOptions.states[this.reminderOptions.triggerOldState] }
        //        }
        //        ....
        //    }, 3000);
        //----------------------------------

        on(this.reminderOptions.triggeredByState, (obj) => {
            log(JSON.stringify(obj));            
            if (obj.state.val == this.reminderOptions.states[this.reminderOptions.triggerState] && obj.oldState.val == this.reminderOptions.states[this.reminderOptions.triggerOldState]) {
                var currentDateTime = new Date();
                if (this.inNoReminderPeriod(currentDateTime, this.reminderOptions.noReminderPeriods)) {
                    log("Reminder not set: inNoReminderPeriod == true : currentDateTime = " + currentDateTime);
                } else {
                    log("Reminder set: " + this.getReminderTime(this.reminderOptions.delayInMinutes) + ";" + this.reminderOptions.message);
                    this.createReminder();
                }
            }
        });
    }

    setUserdataState(stateValue) {

        // this is only to prevent overriding any object by chance
        const fixedPart = "0_userdata.0"
        let varPart = this.reminderOptions.triggeredByState.substring(fixedPart.length + 1, this.reminderOptions.triggeredByState.length)
        let state = fixedPart + "." + varPart
        
        if (!existsState(state)) {            
            createState(state, stateValue, () => { log("State created: " +  state + "=" + stateValue );})
        } else {
            setState(state, stateValue, true, () => { log("State set: " +  state + "=" + stateValue );})        
        }    
    }

    getReminderTime(delayInMinutes) {
        var date = new Date();
        date.setMinutes(date.getMinutes() + delayInMinutes);
        var hourStr = ("0" + date.getHours()).slice(-2);
        var minutesStr = ("0" + date.getMinutes()).slice(-2);
        return hourStr + ":" + minutesStr;
    }

    createReminder() {
        let reminderNew = this.reminderOptions.echoObjectId + "." + this.reminderTags.id + "." + this.reminderTags.newFnId;  
        setState(
            reminderNew,
            this.getReminderTime(this.reminderOptions.delayInMinutes) + "," + this.reminderOptions.message,
            false,
            () => {
                log(
                "setState: " + reminderNew + " complete"
                );
            }
        );
    }

    // Provide current Date() and the periods as array of objects with begin and end [{ begin: '12:00', end: '13:00'}, { begin: '22:00', end: '04:30'}]
    inNoReminderPeriod(currentDateTime, periods) {
        var ret = false;
        var startOfDay = new Date().setHours(0, 0, 0, 0);
        var endOfDay = new Date().setHours(23, 59, 59, 59);

        periods.forEach((period) => {
            var beginArr = period.begin.split(":");
            console.log(beginArr);
            var beginOfPeriod = new Date(
                currentDateTime.getFullYear(),
                currentDateTime.getMonth(),
                currentDateTime.getDate(),
                ...beginArr
            );
            var endArr = period.end.split(":");
            var endOfPeriod = new Date(
                currentDateTime.getFullYear(),
                currentDateTime.getMonth(),
                currentDateTime.getDate(),
                ...endArr
            );

            if (currentDateTime >= beginOfPeriod && currentDateTime <= endOfPeriod) {
                ret = true;
            } else {
                if (beginOfPeriod > endOfPeriod) {
                    if (
                        (currentDateTime >= startOfDay && currentDateTime <= endOfPeriod) ||
                        (currentDateTime >= beginOfPeriod && currentDateTime <= endOfDay)
                    ) {
                        ret = true;
                    }
                }
            }
        });
        return ret;
    }
}

var wamaReminder = new WamaReminder(wamaReminderOptions);
