from _test_context_ import test_context
import cv2
import glob 
import random
import os
from pkg_logger import Logger

class WebcamMock:
    def __init__(self, round_filters = []):
        """
            round_filters let's you define your own image sequence
            [{ "to_round": 5, "label": "00_10plus" }, { "to_round": 18, "label": "01_9min" }, { "to_round": 22, "label": "02_off" }] 
            image 1-5: return images from folder 00_10plus
            image 6-18: return images from folder 01_09min
            image 19-22: return images from folder 02_off
            image 23: return none
        """
        self.logger = Logger().attach()
        self.round = 0        
        self.round_filters = [{ "to_round": 5, "label": "02_off" }, { "to_round": 10, "label": "00_10plus" }, { "to_round": 15, "label": "01_9min" }, { "to_round": 20, "label": "02_off" }]
        if len(round_filters) > 0:
            self.round_filters = round_filters
        self.images = []
        self.image_path = test_context.mock_image_folder
        for filename in glob.iglob(str(self.image_path) + '/**/*.jpg', recursive=True):
            self.images.append(filename)

    def capture(self):
        """
            Returns cv2-image from cv2 VideoCapture in BGR-format. Use cv2.cvtColor(image, cv2.COLOR_BGR2RGB) to convert to RGB format
        """
        self.round += 1
        filter_str = ""
        for round_filter in self.round_filters:
            if self.round <= round_filter["to_round"]:
                filter_str = round_filter["label"]
                break
        if filter_str != "":
            filtered_images = list(filter(lambda img: img.find(filter_str) >= 0, self.images))
            random_image = random.choice(filtered_images)
            self.logger.debug(f'Mock-image captured: {random_image}')
            cv2_image = cv2.imread(random_image)
            # ret, image = cv2.imencode(".JPEG", cv2_image)
            return cv2_image
        else:    
            return None
