import time
import socket
import threading
from pkg_logger import Logger

# Check used ports: <user@host>:~$ netstat -tulpn 
# Send commands to server with nc = netcat or use sockt_client.py in this directory: 
# <user@host>:~$ nc 127.0.0.1 65111 
# Hello<enter>
# OK
# ^C

HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
PORT = 65111  # Port to listen on (non-privileged ports are > 1023)

class SocketServer:
    def __init__(self, host = HOST, port = PORT, cmd_received = None):
        self.host = host
        self.port = port
        self.logger = Logger().attach()
        self.lock = threading.Lock()
    
        try: 
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.logger.info(f"Binding to host: {self.host} port: {self.port}")
            self.socket.bind((self.host, self.port))
            self.socket.listen()
        except Exception as e:
            self.logger.error(e)

        self.conn = None

        self.on_cmd_received = cmd_received
        return

    def run_async(self):
        self.receive_thread = threading.Thread(target = self.receive, args=[])
        self.receive_thread.start()

    def receive(self):
        while True:
            try:
                if not self.conn:
                    self.conn, self.addr = self.socket.accept()
                data = self.conn.recv(1024)
                if not data:
                    self.conn = self.conn.close()  
                else:
                    recv_str = data.decode("utf-8")
                    recv_str = recv_str.rstrip() # remove newlines from nc
                    OK = False
                    ret_str = "Unknown command: " + recv_str
                    if self.on_cmd_received:
                        self.lock.acquire()
                        OK = self.on_cmd_received(recv_str)
                        self.lock.release()
                    if OK:
                        ret_str = "OK"
                    ret_str = ret_str + "\n" # add newline for nc
                    self.conn.sendall(ret_str.encode("utf-8")) 
                    self.logger.debug(recv_str + " -> " + ret_str)
            except socket.error as e:
                # this occurs when socket.close() is called while waiting in socket.accept() or conn.recv()
                # break stops the loop and finishes the thread
                # self.logger.debug(e) -> [Errno 22] Invalid argument 
                break        
        return        
  
    def stop(self):
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()       
            self.receive_thread.join()
        except Exception as e:
            self.logger.debug(e)    
        finally:
            self.logger.debug("Connection closed")

def main():
    ss = SocketServer()
    ss.run_async()
    try: 
        while True:
            time.sleep(60)            
    except KeyboardInterrupt:
        ss.logger.debug("Caught keyboard interrupt, exiting")
    finally:   
        ss.stop()

if __name__ == "__main__": 
    main()
