# 
# see https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/examples/python/label_image.py
#
import numpy as np
# from PIL import Image
import cv2
import tflite_runtime.interpreter as tflite

class TfliteClassifier:
    def __init__(self, model_file):                
        self.interpreter = tflite.Interpreter(model_path=str(model_file))
        self.interpreter.allocate_tensors()
        self.input_details = self.interpreter.get_input_details()
        self.output_details = self.interpreter.get_output_details()

        # see model.json 
        # height = self.input_details[0]['shape'][1]
        # width = self.input_details[0]['shape'][2]
        self.floating_model = (self.input_details[0]['dtype'] == np.float32)
        self.input_mean = 0 
        self.input_std = 255

    def classify(self, image):

        if isinstance(image, str):
            # Using PIL
            # image = Image.open(image_path)
            image = cv2.imread(image)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        else:
            image = image
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        input_data = np.expand_dims(image, axis=0)        

        # Normalize pixel values if using a floating model (i.e. if model is non-quantized)
        if self.floating_model:
            input_data = (np.float32(input_data) - self.input_mean) / self.input_std

        self.interpreter.set_tensor(self.input_details[0]['index'], input_data)
        self.interpreter.invoke()
        output_data = self.interpreter.get_tensor(self.output_details[0]['index'])
        results = np.squeeze(output_data)        
        results = results.astype("float")
        # for result in results:
        #     print(float(result / 255.0))
        # for idx, result in enumerate(results):
        #     results[idx] = float(result / 255.0)        
        if not self.floating_model:
            results /= 255
        return results
        #for i in top_k:
        #    print('{:08.6f}: {}'.format(float(results[i] / 255.0)))
