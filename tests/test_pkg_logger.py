from _test_context_ import test_context
from pkg_logger import Logger

def do_loggings(logger):
    logger.info("Log my Info")
    logger.debug("Log my Debug-Info")
    return


if __name__ == "__main__":

    logger = Logger().get(__name__, True)
    do_loggings(logger)

