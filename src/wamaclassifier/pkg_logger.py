import logging

class Logger():
    def __init__(self):
        self.name = 'debug_logger'

    def get(self, debug=False, logfile=""):
        
        if debug == True:
            log_level = logging.DEBUG
        else:
            log_level = logging.INFO
        
        file_formatter = logging.Formatter('%(asctime)s - %(levelname)8s:  %(message)s [%(module)s.%(funcName)s %(name)s]')
        console_formatter = logging.Formatter('%(asctime)s - %(levelname)8s:  %(message)s [%(module)s.%(funcName)s %(name)s]')
        
        if debug == True and logfile != "":
            file_handler = logging.FileHandler(logfile)
            file_handler.setLevel(logging.DEBUG)
            file_handler.setFormatter(file_formatter)
        console_handler = logging.StreamHandler()
        console_handler.setLevel(log_level)
        console_handler.setFormatter(console_formatter)

        logger = logging.getLogger(self.name)
        if debug == True and logfile != "":
            logger.addHandler(file_handler)
        logger.addHandler(console_handler)
        logger.setLevel(log_level)        
        
        return logger

    def attach(self):
        if not logging.getLogger(self.name).hasHandlers():
            return self.get(True, "")
        else:    
            return logging.getLogger(self.name)
