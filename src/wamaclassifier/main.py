 #! /usr/bin/python3

from distutils.log import debug
import cv2
from webcam_cv2 import Webcam
from debug_command_handler import DebugCommandHandler
from classify_tflite_model import TfliteClassifier
from image_preparation import ImagePreparation
import numpy as np
import time
import requests
import json
import sys
import argparse
from pathlib import Path
from pkg_logger import Logger
from datetime import datetime

class Classifier:

    def __init__(self, settings, debug_command_handler:DebugCommandHandler = None):        
        self.settings = settings        
        self.debug_command_handler = debug_command_handler
        self.logger = Logger().attach()
        self.labels = np.genfromtxt(settings["labels_file"], dtype='U')
        self.classifier = TfliteClassifier(self.settings['model_file'])
        self.ip = ImagePreparation(self.settings["crop_rectangle"]["top"], self.settings["crop_rectangle"]["bottom"], \
                              self.settings["crop_rectangle"]["left"], self.settings["crop_rectangle"]["right"], \
                              self.settings["target_size"]["width"], self.settings["target_size"]["height"])                              
        self.winner_idxes = []
        self.set_camera_interval_seconds(self.settings["initial_idx"])


    def run(self, image):
        self.image = image        
        self.image = self.ip.prepare(self.image)
        winner_idx = self.invoke_classifier() 
        if winner_idx >= 0:
            self.append_winner_idx(winner_idx)
            if self.check_confirm_idx(winner_idx):
                self.set_camera_interval_seconds(winner_idx)
                self.sendStatus(self.labels[winner_idx])
                self.winner_idxes.clear()

    def invoke_classifier(self):
        results = self.classifier.classify(self.image)
        winner_idx = 0
        for idx, result in enumerate(results):
            self.logger.info(self.labels[idx] + ': ' + str(result))
            if result > results[winner_idx]:
                winner_idx = idx                               
        self.debug_write_image(winner_idx)
        if results[winner_idx] >= self.settings["minimum_confidence"]:
            return winner_idx                
        else:
            return -1

    def append_winner_idx(self, idx):
        """check if all winner_idxes are equal - if not remove previous items to start again"""
        self.winner_idxes.append(idx)
        if not self.winner_idxes.count(self.winner_idxes[0]) == len(self.winner_idxes):
            self.winner_idxes.clear()
            self.winner_idxes.append(idx)
        return

    def check_confirm_idx(self, idx):
        confirm_items_filtered = list(filter(lambda item: item["index"] == idx, self.settings["confirm"]))
        if len(confirm_items_filtered) > 0:
            confirm_item = confirm_items_filtered[0]
            repetitions = confirm_item["repetitions"]
            if len(self.winner_idxes) >= repetitions:
                return True
            else:
                return False   
        else:
            return True

    def set_camera_interval_seconds(self, idx):
        camera_interval_seconds = self.get_dict_list_item(self.settings["camera_interval_seconds"], "seconds", idx, 600) 
        self.logger.debug(f'camera_interval_seconds: {camera_interval_seconds} seconds')
        self.camera_interval_seconds = camera_interval_seconds

    def debug_image_required(self, idx):
        image_required = self.get_dict_list_item(self.settings["debug_images_states"], "capture", idx, "False") == "True"
        if self.debug_command_handler and self.debug_command_handler.capture_debug_image() != "":
            image_required = True
            self.debug_command_handler.cmd_ready(self.debug_command_handler.capture_debug_image())
        self.logger.debug(f'debug_image_required: { str(image_required) }')
        return image_required
        

    def get_dict_list_item(self, dict_list, dict_item, idx, default_value):        
        ''' Returns the value for idx from a list of dictionaries used in settings...json or the value marked as <default>
            Each dictionary in the list must have an index item.
        '''
        default_ret_val = default_value
        items = list(filter(lambda item: item["index"] == idx, dict_list))
        if len(items) > 0:
            return items[0][dict_item]
        else:    
            if idx != "<default>":
                idx = "<default>" # value for any other indices not explicitely given
                items = list(filter(lambda item: item["index"] == idx, dict_list))
                if len(items) > 0:
                    return items[0][dict_item]
            return default_ret_val

    def debug_write_image(self, winner_idx):
        if (self.settings["debug"] and self.debug_image_required(winner_idx)):
            # when we are in debug mode we possibly want to save the image in a folder under <debug_log_path>/debug_images/<label>    
            debug_images_label_folder = Path(self.settings["debug_images_path"]).joinpath(self.labels[winner_idx])
            if not debug_images_label_folder.exists():
                # read, write, list files for owner, read/list files for groups and others
                try:
                    debug_images_label_folder.mkdir(mode=0o755)
                except Exception as e:
                    self.logger.error(e)                    
            if debug_images_label_folder.exists():
                debug_image_filename = datetime.now().strftime("%Y-%m-%d_%H-%M-%S_%f") + ".jpg"
                cv2.imwrite(str(debug_images_label_folder.joinpath(debug_image_filename)), self.image)
        

    def sendStatus(self, label):
        # params = { "value" : label}
        ip_address = self.settings["simpleApiAddress"] + "/set/" + self.settings["ioBrokerItem"] + "?value=" + label
        self.logger.debug(ip_address)
        response = requests.get(ip_address)
        self.logger.debug(response.status_code)

def main():
    logfile = "debug.log"

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c',
        '--config_path',
        default="",
        help='config-path containing: model.tflite, labels.txt, settings.json')
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Show debug messages and log to file - set debug_log_path (without filename) in settings.json')    
    parser.add_argument(
        '--mock_webcam',        
        action='store_true',
        help='Use WebcamMock from /test folder - useful for development purposes')
    parser.add_argument(
        '--dev',        
        action='store_true',
        help='True: Use the folders from development environment')


    args = parser.parse_args()

    config_path = ""
    if args.dev == True:
        config_path = str(Path(__file__).parent.parent.parent.joinpath('config').resolve())              
    if args.config_path != "":
        config_path = args.config_path

    if not Path(config_path).exists():        
        sys.exit(f'Invalid config folder: {config_path}')

    # load settings from file    
    settings_filename = "settings.json"
    if args.dev == True:
        settings_filename = "settings_dev.json"
    settings_file = Path(config_path).joinpath(settings_filename).resolve()

    try:
        with open(settings_file, "r") as json_file:
            classifier_settings = json.load(json_file)
    except IOError as e:
        sys.exit(e)

    # config_path must contain model.tflite and labels.txt 
    classifier_settings['model_file'] = str(Path(config_path).joinpath('model.tflite').resolve())
    classifier_settings['labels_file'] = str(Path(config_path).joinpath('labels.txt').resolve())
    if not Path(classifier_settings['model_file']).exists():
         sys.exit('Model file not found: ' + str(classifier_settings['model_file']))
    if not Path(classifier_settings['labels_file']).exists():
         sys.exit('Label file not found: ' + str(classifier_settings['labels_file']))

    # if we want to log debug messages and save images?
    classifier_settings['debug'] = False
    debug_command_handler = None
    if args.debug == True:
        classifier_settings['debug'] = True
        if classifier_settings['debug_log_path'] == '':
            classifier_settings['debug_log_path'] = str(Path(config_path))
        debug_images_path =  Path(config_path).joinpath('debug_images')
        # read, write, list files for owner, read/list files for groups and others
        try:
            debug_images_path.mkdir(exist_ok=True, mode=0o755)
        except Exception as e:
            sys.exit(e)
            
        classifier_settings['debug_images_path'] = str(debug_images_path)
        if not 49152 <= classifier_settings['debug_command_server_port'] <= 65535:
            sys.exit(f"Port ot in range: 49152-65535: {classifier_settings['debug_command_server_port']}")

        debug_command_handler = DebugCommandHandler(port=classifier_settings['debug_command_server_port'])

    logger = Logger().get(args.debug == True, str(Path(classifier_settings['debug_log_path']).joinpath(logfile).resolve()))

    classifier = Classifier(classifier_settings, debug_command_handler)
    
    if (args.mock_webcam == True):
        test_folder = Path(__file__).parent.parent.parent.joinpath('tests').resolve()
        sys.path.append(str(test_folder))
        from webcam_mock import WebcamMock
        # Add your own image sequence, see tests/webcam_mock.py
        logger.debug(classifier_settings["webcam_mock_round_filters"])
        cam = WebcamMock(round_filters=classifier_settings["webcam_mock_round_filters"]) 
    else:
        cam = Webcam()

    try:

        if classifier_settings['debug'] == True:
            debug_command_handler.run_async()

        while True:
            image = cam.capture()
            if image is not None:
                if (args.mock_webcam == True):                    
                    logger.debug('Mock image captured.')
                else:
                    logger.debug('Camera image captured.')
                classifier.run(image)
                
                sleep_time = 0
                if classifier_settings['debug'] == True:
                    while sleep_time <= classifier.camera_interval_seconds:
                        if debug_command_handler.capture_debug_image() != "":
                            break                                        
                        time.sleep(1)
                        sleep_time = sleep_time + 1
                else:
                    time.sleep(classifier.camera_interval_seconds)
            else: 
                logger.info("image = cam.capture(): No image received.")
                break
    except Exception as e:
        logger.error(e)
    finally:
        if debug_command_handler:
            debug_command_handler.stop() 


if __name__ == "__main__": 
    main()
    
