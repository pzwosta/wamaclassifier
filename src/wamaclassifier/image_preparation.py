import cv2

class ImagePreparation:
    def __init__(self, crop_top, crop_bottom, crop_left, crop_right, resize_width, resize_height):
        self.crop_top = crop_top
        self.crop_bottom = crop_bottom
        self.crop_left = crop_left
        self.crop_right = crop_right
        self.resize_width = resize_width
        self.resize_height = resize_height        
    
    def prepare(self, image):
        """ 
            Crops and resizes an image            
            image:  filepath or cv2::image
            return: cv2::image
        """
        if isinstance(image, str):
            cv2_image = cv2.imread(image)
        else:
            cv2_image = image
        cropped_image = self.crop_image(cv2_image, self.crop_top, self.crop_bottom, self.crop_left, self.crop_right)
        prepared_image = self.resize_image(cropped_image, self.resize_width, self.resize_height)
        # cv2.imwrite(image_filename, prepared_image)            
        return prepared_image

    # we want the image to be as large as possible without losing any information
    def crop_image(self, cv2_image, top, bottom, left, right):
        return cv2_image[top:bottom, left:right]

    def resize_image(self, cv2_image, width, height):    
        scale = min(height/cv2_image.shape[0], width/cv2_image.shape[1]) 
        new_height = int(cv2_image.shape[0] * scale)
        new_width = int(cv2_image.shape[1] * scale)    
        dim = (new_width, new_height)
        cv2_image = cv2.resize(cv2_image, dim, interpolation= cv2.INTER_AREA)
        dim = (max(cv2_image.shape[1], width), max(cv2_image.shape[0], height))
        cv2_image = cv2.resize(cv2_image, dim, interpolation= cv2.INTER_AREA) 
        return cv2_image

