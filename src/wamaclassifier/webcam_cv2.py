import cv2 
from pkg_logger import Logger
# import glob 
# import random
# import os

class Webcam:
    def __init__(self, video_nr=0):
        ''' Init video-source: default /dev/video0 
            v4l2-ctl --list-devices shows your devices. 
            apt install v4l-utils if v4l2-ctl is unknown
        '''
        self.logger = Logger().attach()
        self.video_nr = video_nr
        # Move to self.capture() to make cam for other apps available?
        # -> Let the app decide that uses this class
        self.cam = cv2.VideoCapture(video_nr)
        

    def capture(self):
        """
            Returns cv2-image from cv2 VideoCapture in BGR-format. Use cv2.cvtColor(image, cv2.COLOR_BGR2RGB) to convert to RGB format
        """
        try:
            ret, frame = self.cam.read()            
            # ret, image = cv2.imencode(".JPEG", frame)            
            # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) # Converting BGR to RGB
            return frame
        except Exception as e:
            self.logger.error(e)
            return None    

    def __del__(self):
        # Move to self.capture() to make cam for other apps available?
        # We let the app decide that uses this class
        self.cam.release()
        return
