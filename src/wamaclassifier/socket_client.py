import socket
import sys
from pkg_logger import Logger
import argparse

# usage:
# python socket_client.py <port>
#
# Alternatively use the netcat utility: nc <host> <port>
#
# <user@host>:~$ nc 127.0.0.1 65111 
# Hello<enter>
# OK
# ^C

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 65111  # The port used by the server

class SocketClient:
    def __init__(self, host = HOST, port = PORT):
        self.host = host
        self.port = port
        self.logger = Logger().attach()
        self.conn = None
        return

    def run(self):
        try: 
            self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.conn.connect((self.host, self.port))
            print(f"Send debug commands to wamaclassifier (host: {self.host} port: {self.port})\n- 'exit + <Enter>' to quit and disconnect. \n- Press <Enter> to repeat previous command\n")
            prev_line = ""
            while True:
                # line = sys.stdin.readline().rstrip()                
                line = input()                
                if line == "" and prev_line != "":
                    line = prev_line
                    print(line)
                if (line == "" and prev_line == "") or line =="quit" or line == "exit" or line == "q":
                    break
                prev_line = line
                self.conn.sendall(line.encode("utf-8"))
                data = self.conn.recv(1024)
                print(data.decode("utf-8").rstrip())
        except Exception as e:
            self.logger.error(e)

    def stop(self):
        self.conn.close()

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=int, nargs="?", default=PORT, help="Port the socket-server is listening to. Default=65111 (Host=127.0.0.1)")
    args = parser.parse_args()
    # print(args)

    sc = SocketClient(port=args.port)
    sc.run()
    sc.stop()

if __name__ == "__main__": 
    main()

