from socket_server import SocketServer
import time
from pkg_logger import Logger
import argparse

class DebugCommandHandler:
    def __init__(self, port = None):
        self.debug_socket_server = SocketServer(port=port, cmd_received=self.on_cmd_received)
        # cap: skip sleep_time, capture image classify it and save to debug_images_path 
        self.valid_commands = ["cap"]
        self.received_commands = []
        self.logger = Logger().attach()
    def run_async(self):
        self.debug_socket_server.run_async()
    def on_cmd_received(self, cmd):
        if self.valid_commands.count(cmd) > 0:
            self.received_commands.append(cmd)
            return True
        return False    
    def capture_debug_image(self):
        cmd = "cap"        
        ret = ""
        # reading received_commands while possibly on_cmd_received writes to it - see socket_server receive
        self.debug_socket_server.lock.acquire()
        if self.received_commands.count(cmd) > 0:
            ret = cmd
        self.debug_socket_server.lock.release()
        return ret
    def cmd_ready(self, cmd):
        self.debug_socket_server.lock.acquire()
        if self.received_commands.count(cmd) > 0:
            self.received_commands.remove(cmd)
        self.debug_socket_server.lock.release()

    def stop(self):
        self.debug_socket_server.stop()

def main():

    PORT=65111

    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=int, nargs="?", default=PORT, help="Port the socket-server is listening to. Default=65111 (Host=127.0.0.1)")
    args = parser.parse_args()

    dch = DebugCommandHandler(port=args.port)
    dch.run_async()
    try: 
        while True:
            time.sleep(60)            
    except KeyboardInterrupt:
        dch.logger.debug("Caught keyboard interrupt, exiting")
    finally:   
        dch.stop()

if __name__ == "__main__": 
    main()
