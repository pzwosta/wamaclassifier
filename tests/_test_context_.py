from pathlib import Path
import sys

class TestContext:
    def __init__(self):
        self.test_folder = Path(__file__).parent.resolve()
        self.project_root = self.test_folder.parent.resolve()
        self.src_folder = self.project_root.joinpath('src/wamaclassifier').resolve()
        self.image_base_folder = self.project_root.joinpath('tflite-model-maker').resolve()
        self.mock_image_folder = self.image_base_folder.joinpath('wama-camera-labeled-640x480').resolve()
        sys.path.append(str(self.src_folder))

test_context = TestContext()
