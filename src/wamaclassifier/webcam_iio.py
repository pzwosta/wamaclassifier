# pip install imageio imageio-ffmpeg
import imageio as iio

class Webcam:
    def __init__(self, video_nr=0):
        try: 
            video = "<video" + str(video_nr) + ">"
            self.cam = iio.get_reader(video)
        except Exception as e:
            print(e)

    def capture(self):
        """
            Returns image from imageio VideoCapture in RGB-format. 
        """
        try:
            frame = self.cam.get_data(0)
            return frame
        except Exception as e:
            print(e)
            return None    

    def __del__(self):
        self.cam.close()            

