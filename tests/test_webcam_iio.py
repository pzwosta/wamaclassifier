from _test_context_ import test_context
from webcam_iio import Webcam
import imageio as iio
import time

def main():

    try:
        cam = Webcam(1)  # need to use /dev/video1 in virtalbox image
        while True:
            time.sleep(5)
            image = cam.capture()
            if image is not None:
                print(time.strftime("%H:%M:%S"), ' Camera image captured: ')
                iio.imwrite(str(test_context.test_folder.joinpath("test_webcam_image.jpeg").resolve()), image)
            else: 
                print("image = cam.capture(): No image received.")
                break
    except Exception as e:
        print(e)

if __name__ == "__main__":
    main()

