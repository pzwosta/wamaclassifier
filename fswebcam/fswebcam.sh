#!/bin/bash

# This script calls fswebcam with the specified settings to capture an image
# Good description how to install and use the fswebcam package
# https://www.raspberrypi.com/documentation/computers/os.html#using-a-usb-webcam
# fswebcam manual: http://manpages.ubuntu.com/manpages/bionic/man1/fswebcam.1.html
#
# Install the fswebcam-package: sudo apt install fswebcam
# Copy this script to your /bin-folder next to the fswebcam binary if you want
# 
# Call the script once to test your webcam and possibly some extra setting or
# call it regularly through a cron job (see the example in crontab)
# 
# For exta settings for your webcam use v4l2-ctl, see
# http://manpages.ubuntu.com/manpages/bionic/man1/v4l2-ctl.1.html 
# 
# v4l2-ctl -d /dev/video0 -c exposure_auto=1
# v4l2-ctl -d /dev/video0 -c exposure_absolute=120
# sleep(2)
# 
# Sample code to capture an image, save with a unique name derived from current date and time and copy it to latest.jpg
# You have to create the folder /srv/fswebcam-images manually
#
# To check the images use the python 3 built in Http-Server from that folder: python -m SimpleHTTPServer 8083 
# and send your browser to the address of your server http://xxx.xxx.xxx.xxx:8083

name=`date +'%Y-%m-%d_%H%M%S'`
fswebcam -r "640x480" --no-banner -v /srv/fswebcam-images/$name.jpg
cp /srv/fswebcam-images/$name.jpg /srv/fswebcam-images/latest.jpg

